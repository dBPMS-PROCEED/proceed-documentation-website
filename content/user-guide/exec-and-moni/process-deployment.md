+++
title = "Process Deployment"
menuTitle = "Process Deployment"
date =  2019-04-19T18:47:31+02:00
draft = true
hidden = false
weight = 10
+++

There are two different methods for deployment: Static and Dynamic. For an explanation, please see [here]({{< ref "concepts/deployment/_index.md" >}}).

## The Device and Capability List

The Management System has a _Devices_ view where you can find every machine that runs a PROCEED Engine and is connected to the local network.
All PROCEED Engines are automatically discovered with mDNS. If you installed an engine on a system that is not displayed in the view (maybe because it is not in the LAN), you can add it manually. This list is the foundation for the deployment of processes.
