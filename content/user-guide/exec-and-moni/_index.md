+++
title = "Process Deployment, Execution, Monitoring and Administration"
menuTitle = "Execution & Monitoring"
chapter = true
date =  2019-04-19T15:09:47+02:00
draft = false
hidden = false
weight = 30
+++

# Process Deployment, Execution, Monitoring and Administration

In this section you can find information how to manage the execution of processes in PROCEED.