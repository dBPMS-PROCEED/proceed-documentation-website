+++
title = "Process Modelling"
menuTitle = "Process Modelling"
chapter = true
date =  2019-04-19T14:51:24+02:00
draft = false
hidden = false
weight = 20
+++

# Process Modelling

Here you can find how to create processes in PROCEEDs Management System.

{{% children description="true" %}}
