+++
title = "User Assignment"
menuTitle = "User Assignment"
date =  2019-04-19T18:57:19+02:00
draft = true
hidden = false
weight = 40
+++



**Management-System Interface**
Once a Task has been marked as a User Task  the icon 'Select Users/Roles' appears which allows you 
to add multiple local/remote users/roles to a User Task:
