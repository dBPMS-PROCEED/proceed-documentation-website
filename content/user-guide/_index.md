+++
title = "Creating Processes in PROCEED"
menuTitle = "User Guide"
date = 2019-04-01T21:36:52+02:00
weight = 40
chapter = true
hidden = false
draft = false
+++


# Creating Processes in PROCEED

This Guide explains every detail about the possibilities in PROCEED's Management System (MS) and PROCEED's Engine (DPE).