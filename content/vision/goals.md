+++
title = "Goals"
date =  2018-09-28T19:10:20+02:00
draft = false
hidden = false
weight = 10
+++

The main goal of the PROCEED project is to spread the process execution so that processes are executed on multiple, distributed process engines without a connection to a coordinating server or management system.
<!-- This leads to the distribution of the process engine in contrast to typical Client-Server systems. -->

<!-- Anmerkungen Andreas Kronz bei Scheer Treffen:
- mehr Blockchain Wording anschauen und benutzen
- wie passt die Engine in die etablierten Prozessumgebungen/Prozesse (ERP-Systeme)?
- wo passt die Engine rein (Prozessorientierte IoT-Steuerungsplattform unabhängig von etablierten IoT Lösungen anderer Anbieter ) -->

**IoT Integration**:
: One of the main goals is the coordination of multiple IoT devices in a process.

{{% expand "Detailed Description" %}}
- IoT devices run on a multitude of different platforms. The PROCEED execution engine offers a generic architecture to support a big variety of platforms.
- IoT devices are constraint systems with sensors and actuators that offer a wide variety of capabilities from regulating the heating to printing 3D figures.
{{% /expand %}}
  
**Fog Processes**:
: Process steps are directly executed on the involved or nearby devices.

<!-- {{% expand "Detailed Description" %}}

{{% /expand %}} -->
  
**Portable Processes**:
: Processes can be transported to and continued in other environments.

<!-- {{% expand "Detailed Description" %}}
{{% /expand %}} -->
  
**Interoperable Processes**:
: Using and defining standards for executing distributed processes so that it can run on every compliant process engine.

<!-- {{% expand "Detailed Description" %}}
 - was hat das für eine Konsequenz: Capability, Prozessweitergabe, Lokale Ausführung - Autonom? 
{{% /expand %}} -->
  
**Context-aware Processes**:
: Depending on the availability of (surrounding) systems, the process execution can adapt to current circumstances.

<!-- {{% expand "Detailed Description" %}}

{{% /expand %}} -->
  
**Privacy-aware Processes**:
: Processes that involve private data respect privacy profiles of process participants.
<!-- e.g. by partly process data on site, removing the need to transfer all data to an external service. -->

<!-- {{% expand "Detailed Description" %}}

{{% /expand %}} -->
  
**Enterprise-aware Processes**:
: Parts of a process can be created by different companies and integrated into one process without leaking internal process details.

<!-- {{% expand "Detailed Description" %}}

{{% /expand %}} -->
  
**Smart process interaction**:
:  PROCEED offers new ways to create and interact with processes.

<!-- {{% expand "Detailed Description" %}}

{{% /expand %}} -->