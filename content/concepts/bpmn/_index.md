+++
title = "BPMN Adaptions"
date = 2019-01-28T21:29:58+01:00
weight = 90
chapter = true
hidden = false
draft = false
+++


# BPMN Adaptions

On the following sites the changes and implementations of the BPMN specification are documented. 
Sometimes that was necessary to fulfill the goals of PROCEED, and sometimes it is usual for a process engine to use its own values.

