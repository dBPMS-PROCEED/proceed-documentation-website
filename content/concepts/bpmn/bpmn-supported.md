+++
title = "Supported BPMN Elements"
menuTitle = "Supported Elements"
date =  2019-01-29T22:48:41+01:00
draft = false
hidden = false
weight = 1
+++

## List of tested und usable BPMN elements

| BPMN Element | Usage |
| --- | --- |
| <img src="/images/bpmn/icons-black/EventStartEmpty.svg" style="margin: 0 auto;"> | Empty Start Event: manual start of a process |
| <img src="/images/bpmn/icons-black/EventIntermediateTimer.svg" style="margin: 0 auto;"> | [Intermediate Timer Event]({{% ref "bpmn-timer.md" %}}): delays the execution by a specified time. |
| <img src="/images/bpmn/icons-black/EventEndEmpty.svg" style="margin: 0 auto;"> | Empty End Event: consumes one token. A process is ended if all tokens are consumed. |
| <img src="/images/bpmn/icons-black/EventEndTerminate.svg" style="margin: 0 auto;"> | Terminate End Event: stops the complete process and consumes all tokens. (Currently only possible on one Engine.) |
||
| <img src="/images/bpmn/icons-black/GatewayParallel.svg" style="margin: 0 auto;"> | [Parallel Gateway (AND)]({{% ref "bpmn-gateways.md#and-gateway" %}}) | 
| <img src="/images/bpmn/icons-black/GatewayExclusive.svg" style="margin: 0 auto;"> | [Exclusive Gateway (XOR)]({{% ref "bpmn-gateways.md#xor-and-or-splitting-gateways" %}}) | 
| <img src="/images/bpmn/icons-black/GatewayInclusive.svg" style="margin: 0 auto;"> | [Inclusive Gateway (OR)]({{% ref "bpmn-gateways.md#xor-and-or-splitting-gateways" %}}) |
||
| <img src="/images/bpmn/icons-black/TaskUser.svg" style="margin: 0 auto;"> | [User Task]({{< relref "bpmn-user-tasks.md" >}}): display HTML pages in a task list. |
| <img src="/images/bpmn/icons-black/TaskScript.svg" style="margin: 0 auto;"> | [Script Task]({{< relref "bpmn-script-task.md" >}}): execute Scripts on the Engine. |
| <img src="/images/bpmn/icons-black/TaskSubprocess.svg" style="margin: 0 auto;"> | [Subprocess]({{< relref "bpmn-subprocesses.md" >}}): structure a process with embedded processes. |
| <img src="/images/bpmn/icons-black/TaskCallActivity.svg" style="margin: 0 auto;"> | [Call Activities]({{< relref "bpmn-subprocesses.md" >}}): structure a process by calling other processes. |
||
| <img src="/images/bpmn/icons-black/EventIntError.svg" style="margin: 0 auto;"> | Attached [Boundary Error Event]({{% ref "bpmn-error-escalation.md" %}}). (Only on Scripts Tasks currently.) |
| <img src="/images/bpmn/icons-black/EventIntEscalationInterrupting.svg" style="margin: 0 auto;"> | Attached [Boundary Escalation Event (Interrupting)]({{% ref "bpmn-error-escalation.md" %}}). (Only on Scripts Tasks currently.) |
| <img src="/images/bpmn/icons-black/EventIntEscalationNoninterrupting.svg" style="margin: 0 auto;"> | Attached [Boundary Escalation Event (Non-Interrupting)]({{% ref "bpmn-error-escalation.md" %}}). (Only on Scripts Tasks currently.) |
| <img src="/images/bpmn/icons-black/EventBoundaryTimer.svg" style="margin: 0 auto;"> | Attached [Timer Event (Interrupting)]({{% ref "bpmn-timer.md" %}}). |
| <img src="/images/bpmn/icons-black/EventBoundaryTimerNonInterrupting.svg" style="margin: 0 auto;"> | Attached [Timer Event (Non-Interrupting)]({{% ref "bpmn-timer.md" %}}). |
