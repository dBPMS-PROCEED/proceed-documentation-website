+++
title = "Process Deployment and Execution"
menuTitle = "Deployment and Execution"
date =  2019-01-20T12:50:53+01:00
draft = false
hidden = false
weight = 20
+++

Processes are created inside the Management-System. For a decentralized execution the process steps needs to be transfered to multiple network-connected systems  with an installed distributed process engine. The PROCEED BPMS supports *2 types of deployment*: a static deployment and a dynamic deployment. 

## Static Deployment

Static deployment means that the created process (the so-called *parent*-process) is separated into several, smaller *child* processes. This is done by the process creator at design-time inside the management system. 
Every child process is then assigned to an IT system, which runs the PROCEED engine and has the necessary capability.

Afterwards the deployment can be done: every child is transferred to the assigned system and waits for executing its part of the entire parent process.
The process starts on a system that contains a start event and can be triggered manually (e.g. in case of a BPMN empty start event with the Management System via the API) or automatically (if a catching event waits for the trigger event).


## Dynamic Deployment

Dynamic deployment means that the created process is deployed to a device which is capable of running the _first_ process step(s). After each step the process engine evaluates if it is able to run the next step or if it needs to send the packaged process to another engine that is capable of executing it. This further deployment can also be enforced due to other constraints, e.g. the physical location or workload. 
This means one process literally _jumps_ from machine to machine, and thereby can even travel above network boundaries, if it is transported with a portable device. This is done until every process instance reaches its ends.



