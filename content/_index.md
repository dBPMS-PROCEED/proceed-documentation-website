+++
title = "PROCEED Documentation"
date = 2018-08-28T20:15:03+02:00
weight = 5
# Table of content (toc) is enabled by default. Set this parameter to true to disable it.
# Note: Toc is always disabled for chapter pages
disableToc = "false"
# If set, this will be used for the page's menu entry (instead of the `title` attribute)
menuTitle = ""
# The title of the page in menu will be prefixed by this HTML content
pre = ""
# The title of the page in menu will be postfixed by this HTML content
post = ""
# Set the page as a chapter, changing the way it's displayed
chapter = true
# Hide a menu entry by setting this to true
hidden = false
# Display name of this page modifier. If set, it will be displayed in the footer.
LastModifierDisplayName = ""
# Email of this page modifier. If set with LastModifierDisplayName, it will be displayed in the footer
LastModifierEmail = ""
+++

# PROCEED Documentation

Decentralized execution of individual and interoperable IoT processes.
<!-- Easily create and deploy individual, interoperable, IoT-ready processes in a resilient execution environment. -->

---

PROCEED (PROCess EnginE in a Distributed environment) is a _decentralized Business Process Management System (dBPMS)_ with the goal of developing a process engine that is not centrally running on one Server.
It aims to distribute the execution of process steps directly to all kinds of heterogeneous IoT Machines.
This concept is aligned with the recent evolution of IT systems to switch from a centralized client-server architecture to a decentralized peer-to-peer network, whereby devices of all sizes and operating systems in the vicinity are connected and exchange data in order to fulfill a task.
This can have many advantages, for example:

- _Offline Execution_: Executing a process step directly on a machine and sending the next process step to the next machine, does not require the connection to a central (cloud) server. This can be very interesting for projects with no stable network connection.
- _Network load:_ Sending vast amounts of data over a network (potentially across continents to cloud service providers) causes latency and congestion. By pre-processing the data close to the user these effects are reduced.
- _Smart usage of resources:_ The “Fog/Edge Computing” approach makes sure that resources close to the user are used instead of delegating calculation tasks to a central cloud instance. This helps to ensure an effective use of the available resources and better end user experience.
- _Privacy:_ If sensible data is processed close to the user and not sent to a centralized server, possible misuse of data is avoided and data protection is enhanced.
- _Easy Integrations:_ We offer a Process Engine that can be installed on many different devices. By adhering to the BPMN standard, it is easy to transform existing processes, distribute the execution to different Machines and let them work together. Moreover, we are able to use the capabilities of a Machine in the execution steps, so you don’t need to connect to a heavy or proprietary API.
- _Reliability:_ Instead of having one central point of failure that potentially influence the execution of multiple processes, PROCEED distributes the execution to many Machines. If there is a critical error this can only affect the processes that are currently also executed on the same Machine, and can not influence all other processes.


The project and its software is developed open-source on Gitlab: https://gitlab.com/dBPMS-PROCEED/proceed