+++
title = "Project Information"
date = 2018-09-27T11:47:33+02:00
weight = 50
chapter = true
+++

# PROCEED Project Information

PROCEED is a research project supported by the German government (see [About]({{< relref "about.md" >}})). On the following pages you find management information about the project.