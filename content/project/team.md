+++
title = "Team"
date = 2018-09-27T11:48:53+02:00
draft = false
hidden = false
weight = 20
+++

## Team Members

{{< img src="/images/persons/kai_grunert.png" title="Kai Grunert" >}}
{{< img src="/images/persons/kai_rohwer.jpg" title="Kai Rohwer" >}}
{{< img src="/images/persons/elitsa_pankovska.jpg" title="Elitsa Pankovska" >}}
{{< img src="/images/persons/janis_joderishoferi.jpg" title="Janis Joderishoferi" >}}
{{< img src="/images/persons/lucas_gold.jpeg" title="Lucas Gold" >}}
{{< img src="/images/persons/kevin_hertwig.jpeg" title="Kevin Hertwig" >}}

## Students

{{< img src="/images/persons/a-woman.png" title="Isabelle W." >}}
{{< img src="/images/persons/a-man.png" title="Michael W." >}}
{{< img src="/images/persons/a-woman.png" title="Teresa K." >}}
{{< img src="/images/persons/a-man.png" title="Dimitri S." >}}

{{< img src="/images/persons/pascalis_maschke.png" title="Pascalis Maschke" >}}
{{< img src="/images/persons/manuel_henke.jpg" title="Manuel Henke" >}}
{{< img src="/images/persons/muhammad_abdullah.jpg" title="Muhammad Abdullah" >}}
{{< img src="/images/persons/pavel_dykmann.jpg" title="Pavel Dykmann" >}}
{{< img src="/images/persons/phuong_anh.jpg" title="Phuong Anh" >}}

{{< img src="/images/persons/ziyang_li.jpg" title="Ziyang Li" >}}
{{< img src="/images/persons/Abdullah_Malik.jpg" title="Abdullah Malik" >}}
{{< img src="/images/persons/imke_dungs.jpg" title="Imke Dungs" >}}
{{< img src="/images/persons/linus_pfoch.jpg" title="Linus Pfoch" >}}
{{< img src="/images/persons/santhos_santhanakrishnan.jpg" title="Santhos Santhanakrishnan" >}}

{{< img src="/images/persons/paul_bahmann.jpg" title="Paul Bahmann" >}}
{{< img src="/images/persons/juri_alexander.jpg" title="Juri Alexander" >}}
{{< img src="/images/persons/tomasz_tkaczyk.jpg" title="Tomasz Tkaczyk" >}}
{{< img src="/images/persons/cihan_tas.png" title="Cihan Tas" >}}
{{< img src="/images/persons/beriwan_tepe.jpg" title="Beriwan Tepe" >}}

{{< img src="/images/persons/yannick_milhahn.jpg" title="Yannick Milhahn" >}}
{{< img src="/images/persons/konstanze_segner.jpg" title="Konstanze Segner" >}}
{{< img src="/images/persons/roman_tayupov.jpg" title="Roman Tayupov" >}}
{{< img src="/images/persons/cemre_yuksel.jpg" title="Cemre Yuksel" >}}
{{< img src="/images/persons/alessandro_schneider.jpg" title="Alessandro Schneider" >}}

{{< img src="/images/persons/alexander_zoubarev.jpg" title="Alexander Zoubarev" >}}
{{< img src="/images/persons/victor_purolnik.jpeg" title="Victor Purolnik" >}}
{{< img src="/images/persons/ester_cheynubrata.jpg" title="Ester Cheynubrata" >}}
{{< img src="/images/persons/tarek_higazi.jpg" title="Tarek Higazi" >}}
{{< img src="/images/persons/a-man.png" title="S. C." >}}

{{< img src="/images/persons/michael_narodovitch.jpg" title="Michael Narodovitch" >}}
{{< img src="/images/persons/a-woman.png" title="A. Ö." >}}
{{< img src="/images/persons/rudresha_gulaganjihalli_parameshappa.jpg" title="Rudresha G. Parameshappa" >}}
