# Proceed Documentation

You can find the docs at https://docs.proceed-labs.org 

[Hugo](https://gohugo.io/getting-started/quick-start/) is used as the static site generator with the [Learn theme](https://learn.netlify.com/en/).

If you change something in the Git-Repo, the CI automatically builds and deploys the websites. If you don't want your changes to be immediately published, Hugo offers a *draft* option.



After installing Hugo you have to clone the Git-Repo with the `--recursive` option because  the repo contains git submodules.  
`git clone --recursive https://gitlab.com/dBPMS-PROCEED/proceed-documentation-website.git`  


You can run a local dev server with `hugo server --disableFastRender -D` (includes drafts) and then see your changes under [http://localhost:1313/proceed](http://localhost:1313/proceed)

If you want to add or change something, please make sure that you have [`git lfs`](https://git-lfs.github.com/) installed for storing large files like pictures.


Every content page is located under the `content/` folder. The Learn theme offers two types of pages: a chapter page (just a centered big header) and normal pages. Pages can be arranged in a tree structed with sub-folders. Every sub-folder needs to have one `_index.md` file as the folder homepage (starting point) and can have multiple other markdown pages or sub-folders.

Create a chapter page: `hugo new --kind chapter quick-start/installation/_index.md`  
Create a content page: `hugo new quick-start/installation/requirements.md`

In the beginning of every page you find a section with meta information for the page, surrounded by `---` ([YAML](https://en.wikipedia.org/wiki/YAML) notation) or `+++` ([TOML](https://github.com/toml-lang/toml) notation).
It mostly contains key-value pairs. Here are some important ones (sometimes automatically generated):  

| Metadata | Description |
| --- | ---|
| `draft = true` | Page is a draft and will not be published.  |
| `pre = "<b>1. </b>"` | Text/Symbols in front of the title in the navigation pane  |
| `weight = 5` | Ordering the pages in the navigation pane  |
| `menuTitle = "Install"` | Custom title in the Navigation Pane |
| `disableToc = "true"` | Disable the ToC Symbol in the beginning of a content page |
| `hidden = true` | Hides a content page from displaying in the navigation pane. Links to the page are possible. From Learn theme |

You can find other possible variables in the [Hugo Docs](https://gohugo.io/content-management/front-matter/) oder [Learn Docs](https://learn.netlify.com/en/cont/pages/#front-matter-configuration)

Afterwards you can start creating your pages with Markdown. Here you find a small documentation of Markdown: [https://learn.netlify.com/en/cont/markdown/](https://learn.netlify.com/en/cont/markdown/)  
Since Markdown is sometimes too simple Hugo extends the list of usable command inside the Markdown page with so called [*Shortcodes*](https://gohugo.io/content-management/shortcodes/) `{{% shortcodename parameters %}} TEXT {{% /shortcodename %}}`. Here you find a list of Hugo's Shortcodes: [https://gohugo.io/content-management/shortcodes/#use-hugo-s-built-in-shortcodes](https://gohugo.io/content-management/shortcodes/#use-hugo-s-built-in-shortcodes)  
The Learn theme also defines some shortcodes: [https://learn.netlify.com/en/shortcodes/](https://learn.netlify.com/en/shortcodes/)

Here is a list of the most useful shortcodes:

| Shortcode | Description | Parameters | Doc |
| --- | --- | ---| --- |
| <code>{{%attachments title="Related files" pattern=".*(pdf &#124; mp4)"/%}}</code> | Displays a list of files attached to a page. | title, style, pattern | [Link](https://learn.netlify.com/en/shortcodes/attachments/) |
| `{{% button href="https://getgrav.org/" icon="fas fa-download" icon-position="right" %}}Get Grav with icon right{{% /button %}}` | Nice buttons on your page | href, icon, icon-position | [Link](https://learn.netlify.com/en/shortcodes/button/) |
| `{{% children  %}}` | List the child pages of a page | page, style, showhidden, description, depth, sort | [Link](https://learn.netlify.com/en/shortcodes/children/) |
| `{{%expand "Is this learn theme rocks ?" %}}Yes !.{{% /expand%}}` | Displays an expandable/collapsible section of text on your page |  | [Link](https://learn.netlify.com/en/shortcodes/expand/) |
| `{{<mermaid align="left">}} ... {{< /mermaid >}}` | Generation of diagram and flowchart from text in a similar manner as markdown | align | [Link](https://learn.netlify.com/en/shortcodes/mermaid/) |
| `{{% notice note %}}  A notice disclaimer  {{% /notice %}}` | Disclaimers to help you structure your page | note, info, tip, warning | [Link](https://learn.netlify.com/en/shortcodes/notice/)  |
| `{{< figure src="/media/spf13.jpg" title="Steve Francia" >}}` | extension of the image syntax | src, link, target, rel, alt, title, caption, class, height, width, attr, attrlink | [Link](https://gohugo.io/content-management/shortcodes/#figure) |
| `{{< highlight html >}} ... {{< /highlight >}}` | convert the source code provided into syntax-highlighted HTML | language | [Link](https://gohugo.io/content-management/shortcodes/#highlight) |
| `[Neat]({{< ref "blog/neat.md" >}})  [Who]({{< relref "about.md#who" >}})` | look up the pages by their relative path (e.g., blog/post.md) or their logical name (e.g., post.md) and return the permalink (ref) or relative permalink (relref) for the found page. ref and relref also make it possible to make fragmentary links that work for the header links generated by Hugo | reference | [Link](https://gohugo.io/content-management/shortcodes/#ref-and-relref) |
| `{{< youtube id="w7Ft2ymGmfc" autoplay="true" >}}` | embeds a responsive video player for YouTube videos. | id, autoplay  | [Link](https://gohugo.io/content-management/shortcodes/#youtube) |

# License

The content on this site is licensed under [Creative Commons Attribution 4.0 International (CC BY 4.0)](https://creativecommons.org/licenses/by/4.0/).
